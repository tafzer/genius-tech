
@include('includes.header')
@include('includes.acceuil')
@include('includes.service')
@include('includes.apropos')

<!--<section id="team">
    <div class="container">
        <div class="row">
            <div class="heading text-center col-sm-8 col-sm-offset-2 wow fadeInUp" data-wow-duration="1200ms" data-wow-delay="300ms">
                <h2>Equipe</h2>
                <p>L'equipe est constitué par les membres suivants :</p>
            </div>
        </div>
        <div class="team-members">
            <div class="row">
                <div class="col-sm-3">
                    <div class="team-member wow flipInY" data-wow-duration="1000ms" data-wow-delay="300ms">
                        <div class="member-image">
                            <img class="img-responsive" src="images/team/1.jpg" alt=""> 
                        </div>
                        <div class="member-info">
                            <h3>Mouhamadou M . Diop</h3>
                            <h4>Informaticien</h4>

                        </div>
                        <div class="social-icons">
                            <ul>
                                <li><a class="facebook" href="https://web.facebook.com/tafzer" target="_blank"><i class="fa fa-facebook"></i></a></li>
                                <li><a class="twitter" href="https://twitter.com/tafzer92i" target="_blank"><i class="fa fa-twitter"></i></a></li>
                                <li><a class="linkedin" href="https://www.linkedin.com/in/mouhamadou-moustapha-diop-19857711a/" target="_blank"><i class="fa fa-linkedin"></i></a></li>

                            </ul>
                        </div>
                    </div>
                </div>
                <div class="col-sm-3">
                    <div class="team-member wow flipInY" data-wow-duration="1000ms" data-wow-delay="500ms">
                        <div class="member-image">
                            <img class="img-responsive" src="images/team/2.jpg" alt="">
                        </div>
                        <div class="member-info">
                            <h3>Papa Amadou Sarr</h3>
                            <h4>Informaticien</h4>

                        </div>
                        <div class="social-icons">
                            <ul>
                                <li><a class="facebook" href="#"><i class="fa fa-facebook"></i></a></li>
                                <li><a class="twitter" href="#"><i class="fa fa-twitter"></i></a></li>
                                <li><a class="linkedin" href="#"><i class="fa fa-linkedin"></i></a></li>

                            </ul>
                        </div>
                    </div>
                </div>
                <div class="col-sm-3">
                    <div class="team-member wow flipInY" data-wow-duration="1000ms" data-wow-delay="800ms">
                        <div class="member-image">
                            <img class="img-responsive" src="images/team/3.jpg" alt="">
                        </div>
                        <div class="member-info">
                            <h3>Abdou Khadre Diop</h3>
                            <h4>Informaticien</h4>

                        </div>
                        <div class="social-icons">
                            <ul>
                                <li><a class="facebook" href="#"><i class="fa fa-facebook"></i></a></li>
                                <li><a class="twitter" href="#"><i class="fa fa-twitter"></i></a></li>
                                <li><a class="linkedin" href="#"><i class="fa fa-linkedin"></i></a></li>

                            </ul>
                        </div>
                    </div>
                </div>
                <div class="col-sm-3">
                    <div class="team-member wow flipInY" data-wow-duration="1000ms" data-wow-delay="1100ms">
                        <div class="member-image">
                            <img class="img-responsive" src="images/team/4.jpg" alt="">
                        </div>
                        <div class="member-info">
                            <h3>Abdoulaye Kane</h3>
                            <h4>Informaticien</h4>

                        </div>
                        <div class="social-icons">
                            <ul>
                                <li><a class="facebook" href="#"><i class="fa fa-facebook"></i></a></li>
                                <li><a class="twitter" href="#"><i class="fa fa-twitter"></i></a></li>
                                <li><a class="linkedin" href="#"><i class="fa fa-linkedin"></i></a></li>

                            </ul>
                        </div>
                    </div>
                </div>

                <div class="col-sm-3">
                    <div class="team-member wow flipInY" data-wow-duration="1000ms" data-wow-delay="1100ms">
                        <div class="member-image">
                            <img class="img-responsive" src="images/team/7.jpg" alt="">
                        </div>
                        <div class="member-info">
                            <h3>Abdoulaye Ndigue Sene</h3>
                            <h4>Informaticien</h4>

                        </div>
                        <div class="social-icons">
                            <ul>
                                <li><a class="facebook" href="#"><i class="fa fa-facebook"></i></a></li>
                                <li><a class="twitter" href="#"><i class="fa fa-twitter"></i></a></li>
                                <li><a class="linkedin" href="#"><i class="fa fa-linkedin"></i></a></li>

                            </ul>
                        </div>
                    </div>
                </div>

            </div>
        </div>
    </div>
</section><!--/#team-->
@include('includes.contact')
@include('includes.footer')
