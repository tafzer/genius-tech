<header id="home">
    <div id="home-slider" class="carousel slide carousel-fade" data-ride="carousel">
        <div class="carousel-inner">
            <div class="item active" style="background-image: url({{URL::to ('images/slider/1.jpg') }}">
                <div class="caption">
                    <h1 class="animated fadeInLeftBig">Bienvenue a <span>Genius Tech</span></h1>
                    <p class="animated fadeInRightBig"> Developpement Web - Mobile - Bureau - Responsive Design</p>
                    <a data-scroll class="btn btn-start animated fadeInUpBig" href="#services">Commencez maintenant</a>
                </div>
            </div>
            <div class="item" style="background-image: url({{URL::to ('images/slider/2.jpg') }}">
                <div class="caption">
                    <h1 class="animated fadeInLeftBig">LE voyage vers le <span>Village numérique</span></h1>
                    <p class="animated fadeInRightBig">Accessibilité - Assistance - Sécurité</p>
                    <a data-scroll class="btn btn-start animated fadeInUpBig" href="#services">Commencez maintenant</a>
                </div>
            </div>
            <div class="item" style="background-image: url({{URL::to ('images/slider/3.jpg') }}">
                <div class="caption">
                    <h1 class="animated fadeInLeftBig">La creativite a votre  <span>service</span></h1>
                    <p class="animated fadeInRightBig">Qualité - Innovation - Fiabilité - Solution</p>
                    <a data-scroll class="btn btn-start animated fadeInUpBig" href="#services">Commencez maintenant</a>
                </div>
            </div>
        </div>
        <a class="left-control" href="#home-slider" data-slide="prev"><i class="fa fa-angle-left"></i></a>
        <a class="right-control" href="#home-slider" data-slide="next"><i class="fa fa-angle-right"></i></a>

        <a id="tohash" href="#services"><i class="fa fa-angle-down"></i></a>

    </div><!--/#home-slider-->
    <div class="main-nav">
        <div class="container">
            <div class="navbar-header">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <a class="navbar-brand" href="{{ route('acceuil') }}">
                    <h1><img class="img-responsive" src="images/i.png"  alt="logo"></h1>
                </a>
            </div>
            <div class="collapse navbar-collapse">
                <ul class="nav navbar-nav navbar-right">
                    <li class="scroll active"><a href="#home">Accueil</a></li>
                    <li class="scroll"><a href="#services">Services</a></li>
                    <li class="scroll"><a href="#about-us">A Propos</a></li>
                   <!-- <li class="scroll"><a href="#team">Equipe</a></li>
                    <!--<li class="scroll"><a href="#portfolio">Portfolio</a></li>-->

                    <!-- <li class="scroll"><a href="#blog">Blog</a></li>-->
                    <li class="scroll"><a href="#contact">Contact</a></li>
                </ul>
            </div>
        </div>
    </div><!--/#main-nav-->
</header><!--/#home-->