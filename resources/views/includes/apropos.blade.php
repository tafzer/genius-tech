
<section id="about-us" class="parallax">
    <div class="container">
        <div class="row">
            <div class="col-sm-6">
                <div class="about-info wow fadeInUp" data-wow-duration="1000ms" data-wow-delay="300ms">
                    <h1 style="color: #2e6da4"><span>Genius Tech</span></h1>
                    <p>GeniusTech est une société informatique basée à Dakar et spécialisée dans le développement d'applications web et mobile.
                        Nous concentrons nos efforts dans la transformation digitale des entreprises afin de s'assurer de la qualité de leur production et de leur développement durable.</p>
                        <p>Nous apportons des solutions innovatrices qui permettent à votre structure, votre organisation, votre école… d'intégrer plus facilement le monde numérique.
                        Nous proposons des produits et services de qualité, à prix accessible et adaptés à vos besoins.
                    </p>
                </div>
            </div>
            <div class="col-sm-6">
                <img src="images/logo-genius.png" class="img-responsive">
            </div>
        </div>
    </div>
</section><!--/#about-us-->
<section id="features" class="parallax">
    <div class="container">
        <div class="row count">
            <div class="col-sm-12 col-xs-12 wow fadeInLeft" data-wow-duration="1000ms" data-wow-delay="300ms">
                <span class="fa fa-check-circle fa-5x"></span>
                <br><br>
                <span class="timer" style="font-size: 408%;">100</span> <span style="font-size: 408%;">%</span>
                <br><br>

                <p style="font-size: 608%;">Garantie</p>
            </div>
        </div>
    </div>
</section>
<!--/#features-->