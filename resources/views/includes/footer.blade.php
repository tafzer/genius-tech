<footer id="footer">
    <div class="footer-top wow fadeInUp" data-wow-duration="1000ms" data-wow-delay="300ms">
        <div class="container text-center">
            <div class="footer-logo">
                <a href="{{ url('/') }}"><img class="img-responsive" src="{{URL::to ('images/i2.png') }}" alt=""></a>
            </div>
            <div class="social-icons">
                <ul>
                    <li><a class="envelope" href="#"><i class="fa fa-envelope"></i></a></li>
                    <li><a class="twitter" href="https://twitter.com/GeniusTech3" target="_blank"><i class="fa fa-twitter"></i></a></li>

                    <li><a class="facebook" href="https://www.facebook.com/GeniusTech-1337392972997431/" target="_blank"><i class="fa fa-facebook"></i></a></li>
                    <li><a class="linkedin" href="https://www.linkedin.com/in/genius-tech/Fin de la discussion" target="_blank"><i class="fa fa-linkedin"></i></a></li>
                    <li><a class="dribbble" href="#"><i class="fa fa-youtube"></i></a></li>
                </ul>
            </div>
        </div>
    </div>
    <div class="footer-bottom">
        <div class="container">
            <div class="row">
                <div class="col-sm-6">
                    <p>&copy; 2017 Genius Tech.</p>
                </div>

            </div>
        </div>
    </div>
</footer>

<script type="text/javascript" src="{{URL::to ('js/jquery.js') }}"></script>
<script type="text/javascript" src="{{URL::to ('js/bootstrap.min.js') }}"></script>
<script type="text/javascript" src="http://maps.google.com/maps/api/js?sensor=true"></script>
<script type="text/javascript" src="{{URL::to ('js/jquery.inview.min.js') }}"></script>
<script type="text/javascript" src="{{URL::to ('js/wow.min.js') }}"></script>
<script type="text/javascript" src="{{URL::to ('js/mousescroll.js') }}"></script>
<script type="text/javascript" src="{{URL::to ('js/smoothscroll.js') }}"></script>
<script type="text/javascript" src="{{URL::to ('js/jquery.countTo.js') }}"></script>
<script type="text/javascript" src="{{URL::to ('js/lightbox.min.js') }}"></script>
<script type="text/javascript" src="{{URL::to ('js/main.js') }}"></script>

</body>
</html>